import java.util.Scanner;


public class pattern {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int i,j,k;
		for(i = 0; i <= n; i++) {
			for(j = 0; j < n - i; j++)
				System.out.print("  ");
			k = i;
			while(k > 0) {
				System.out.print(k + " ");
				k--;
			}
			System.out.print("0 ");
			k = 1;
			while(k <= i) {
				System.out.print(k + " ");
				k++;
			}
			System.out.print("\n");
		}
		sc.close();
	}

}
