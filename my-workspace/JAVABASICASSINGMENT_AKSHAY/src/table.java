import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
/**
 * This is table class
 * @author drishti
 *
 */
public class table {
	private static Logger logger = Logger.getLogger(table.class.getName());
	/**
	 * This function is printing the table of number entered in main function.
	 * @param n
	 */
	 static void tablePrint(int n) {
		for(int i = 1; i <= 10; i++)
			logger.info(n + " * " + i + " = "  + (n*i));
	}
	/**
	 * This is our main function.
	 * @param args
	 */
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		try {
			FileHandler fh = new FileHandler("/home/drishti/my-workspace/tablelog.txt");
			logger.addHandler(fh);
			logger.setLevel(Level.FINE);
			SimpleFormatter formatter = new SimpleFormatter();
			fh.setFormatter(formatter);
			}
			catch(Exception e){
			System.out.println(e);
			}
			
			
		
		int n = sc.nextInt();
		/**
		 * This function is called to print th table of numbered entered.
		 */
	    tablePrint(n);
		sc.close();
	}

}
