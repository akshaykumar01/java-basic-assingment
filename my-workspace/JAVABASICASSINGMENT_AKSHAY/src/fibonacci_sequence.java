import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
/**
 * This is fibonacci_sequence class.
 * @author drishti
 *
 */
public class fibonacci_sequence {
	private static Logger logger = Logger.getLogger(fibonacci_sequence.class.getName());
	/**
	 * This is our main function.
	 * @param args
	 */
	public static void main(String args[]) {
		
		try {
			FileHandler fh = new FileHandler("/home/drishti/my-workspace/fibonaccilog.txt");
			logger.addHandler(fh);
			logger.setLevel(Level.FINE);
			SimpleFormatter formatter = new SimpleFormatter();
			fh.setFormatter(formatter);
			}
			catch(Exception e){
			System.out.println(e);
			}
		/**
		 * This loop is used to print fibonacci series.
		 */
		long first = 0, second = 1, sum;
		for(int i = 0; i < 50; i++)
		{
			if(i <= 1) {
				logger.info(i + " ");
			}
			else
			{
				sum = first + second;
				logger.info(sum + " ");
				first = second;
				second = sum;
			}
		}
	}

}
