import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
/**
 * This is fibonacci_sequence_req class.
 * @author drishti
 *
 */
public class fibonacci_sequence_req {
	private static Logger logger = Logger.getLogger(fibonacci_sequence_req.class.getName());
	/**
	 * This function is recursively checking the fibonacci value.
	 * @param n
	 * @return
	 */
	static long fibonacci(long n)
	{   
	    if (n <= 1)
	        return n;
	    return (fibonacci(n-1)+fibonacci(n-2));
	}
	/**
	 * This is our main function.
	 * @param args
	 */
	public static void main(String args[]) {
		
		try {
			FileHandler fh = new FileHandler("/home/drishti/my-workspace/fibonacci_reqlog.txt");
			logger.addHandler(fh);
			logger.setLevel(Level.FINE);
			SimpleFormatter formatter = new SimpleFormatter();
			fh.setFormatter(formatter);
			}
			catch(Exception e){
			System.out.println(e);
			}
		/**
		 * This for loop is checking the fibonacci number at each position.
		 */
		for(int i = 0; i < 50; i++)
		logger.info(fibonacci(i) + " ");
	}

}
