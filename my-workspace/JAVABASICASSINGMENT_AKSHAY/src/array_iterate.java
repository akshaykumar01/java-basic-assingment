import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
/**
 * This is array_iterate class.
 * @author drishti
 *
 */
public class array_iterate {
	private static Logger logger = Logger.getLogger(array_iterate.class.getName());
	/**
	 * This is our main function.
	 * @param args
	 */
	public static void main(String args[]) {
		
		try {
			FileHandler fh = new FileHandler("/home/drishti/my-workspace/arraylog.txt");
			logger.addHandler(fh);
			logger.setLevel(Level.FINE);
			SimpleFormatter formatter = new SimpleFormatter();
			fh.setFormatter(formatter);
			}
			catch(Exception e){
			System.out.println(e);
			}
		
		int arr[] = new int[100];
		/**
		 * This object is created to get random values.
		 */
		Random rn = new Random();
		/**
		 * This loop is assinging random values.
		 */
		for(int i = 0; i < 100; i++)
			arr[i] = rn.nextInt(100);
			/**
			 * this is to print array values using for loop.
			 */
		for(int i = 0; i < 100; i++)
			logger.info(arr[i] + " ");
		
		System.out.print("\n");
		
		int i = 0;
		/**
		 * this is to print array values using while loop.
		 */
		while(i < 100) {
			logger.info(arr[i] + " ");
			i++;
		}
		
		System.out.print("\n");
		
		i = 0;
		/**
		 * this is to print array values using do while loop.
		 */
		do {
			logger.info(arr[i] + " ");
			i++;
		}while(i > 0);
		
		}
}
